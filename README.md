# Ansible-GitLab Hello-World Template

This repository serves as a starting point for users looking to familiarize themselves with the integration of Ansible playbooks and GitLab CI/CD. By leveraging the power of both, you can automate tasks on remote machines directly from GitLab pipelines.

## Overview

- **Ansible**: We use Ansible playbooks to define and run a set of tasks on remote RHEL machines. For demonstration purposes, the tasks are simple commands like `whoami`, `date`, `time`, `ls`, and `hostname`.
  
- **GitLab CI/CD**: The `.gitlab-ci.yml` defines a pipeline that executes the Ansible playbook for each of the mentioned tasks. The output of each task is saved as an artifact for easy viewing and debugging.

## Structure

- `playbook.yml`: This is the Ansible playbook containing tasks for the demo commands.
  
- `.gitlab-ci.yml`: Defines the GitLab CI/CD pipeline. Each stage in the pipeline corresponds to a task in the playbook.

## How to Use

1. **Setup**: Ensure you have a GitLab runner set up with the tag `fedora-runner-shell` and that Ansible is installed on the runner machine.

2. **Inventory File**: Modify the `ANSIBLE_INVENTORY` variable in `.gitlab-ci.yml` to point to your Ansible hosts file.

3. **Triggering Pipelines**: Any push to this repository will trigger the pipeline. Each stage of the pipeline will execute its respective Ansible task.

4. **Viewing Outputs**: After the pipeline completes, navigate to the `Jobs` tab in GitLab. Here, each job (representing a stage) will have its own artifact - a text file with the output of the Ansible command. Click on the "Download" icon next to the job name to download the artifact and view the output.

## Saving Outputs as Artifacts

The `.gitlab-ci.yml` is configured to save the output of each Ansible command as an artifact. This is achieved using the `tee` command, which both displays the command output in the GitLab CI/CD logs and saves it to a text file. These files are then declared as artifacts in the `.gitlab-ci.yml`, making them available for download once the job completes.

## Safety Note

This template disables SSH host key checking for convenience (`ANSIBLE_HOST_KEY_CHECKING: "False"`). While this simplifies running in CI/CD environments, it might introduce potential security risks. Ensure you understand the implications and consider safer alternatives for production setups.

## Conclusion

This template is perfect for those looking to get started with Ansible and GitLab CI/CD. Use it as a springboard for more complex automation and integration tasks!

---

This revised README provides a clear outline of how outputs are saved as artifacts in the GitLab CI/CD pipeline.
